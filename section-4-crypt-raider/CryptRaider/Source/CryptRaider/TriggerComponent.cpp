// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerComponent.h"

UTriggerComponent::UTriggerComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;

}

void UTriggerComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (Mover == nullptr)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s - Mover is a nullptr!"), *GetOwner()->GetActorNameOrLabel());
        return;
    }

    AActor* Actor = GetAcceptableActor();
    if(Actor != nullptr)
    {
        UE_LOG(LogTemp, Display, TEXT("Unlocking"));

        // make sure our actor (gargoyle) is of type UPrimitiveComponent
        UPrimitiveComponent* Component = Cast<UPrimitiveComponent>(Actor->GetRootComponent());
        if (Component != nullptr)
        {
            // and disable physics on the gargoyle so it doesn't get stuck on the floor
            Component->SetSimulatePhysics(false);
        }
        // attach it to the component with the TriggerComponent on it (our secret door)
        Actor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform); // leave it as it is, scale & rotation
        // Actor->AttachToComponent(this, FAttachmentTransformRules::SnapToTargetIncludingScale); // pop straight into alcove, scale to alcove size
        // Actor->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform); // disappears
        // Actor->AttachToComponent(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale); // pop straight into alcove, keep scale

        Mover->SetShouldMove(true);
    }
    else
    {
        // UE_LOG(LogTemp, Display, TEXT("Relocking"));

        Mover->SetShouldMove(false);
    }
}

void UTriggerComponent::SetMover(UMover* NewMover)
{
    Mover = NewMover;
}

AActor* UTriggerComponent::GetAcceptableActor() const
{
    TArray<AActor *> Actors;
    GetOverlappingActors(Actors);

    for (AActor *Actor : Actors)
    {
        if (Actor->ActorHasTag(AcceptableActorTag) && !Actor->ActorHasTag("Grabbed"))
        {
            return Actor;
        }

    }
    return nullptr;
}
