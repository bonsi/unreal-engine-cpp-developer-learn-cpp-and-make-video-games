unreal-engine-cpp-developer

https://gitlab.com/bonsi/unreal-engine-cpp-developer-learn-cpp-and-make-video-games

https://www.udemy.com/course/unrealcourse/

---

# General info & links

- Compile UEditor 5.0 on Linux: https://github.com/EpicGames/UnrealEngine/blob/5.0/Engine/Build/BatchFiles/Linux/README.md
- VSCode/UE Intellisense Fixes Extension: https://gist.github.com/boocs/f63a4878156295b6e854cac68672f305


## Slow editor start on Linux
WIP

doesn't help. still starts on 1 core
```sh
./UnrealEditor ~/online-courses/udemy/unreal-engine-cpp-developer/section-4-crypt-raider/CryptRaider/CryptRaider.uproject -USEALLAVAILABLECORES
```

some output relating startup times
```
[2022.04.27-10.20.28:505][  0]LogUnrealEdMisc: Total Editor Startup Time, took 278.613
[2022.04.27-10.20.33:914][  0]LogLoad: (Engine Initialization) Total time: 284.02 seconds
[2022.04.27-10.20.34:188][  0]LogAssetRegistry: Asset discovery search completed in 249.0305 seconds
```


---
---
---
---
---
---
---


# Section 1: Introduction and Setup

## 5. Build Unreal From Source (Linux)

https://github.com/EpicGames/UnrealEngine

```
sudo apt install g++ git make -y
git clone --single-branch --branch=4.22 git@github.com:EpicGames/UnrealEngine.git UnrealEngine-4.22
cd UnrealEngine-4.22
./Setup.sh (register Unreal file types when asked)
./GenerateProjectFiles.sh
make (takes some time, have 100+ GB free)
cd Engine/Binaries/Linux
./UE4Editor
# Vulkan warning? Update video driver or ./UE4Editor -opengl
```

# Section 2: Triple X - Write Pure C++

## 14. Your First Program
```sh
g++ triplex.cpp -o triplex
```

## 25. Functions

# Section 3: Bulls & Cows

## 39. Editing Actors In Our Level

Moving around:
- right mouse button + wasd (+zoom wheel to inc/dec speed)

Focus on an object:
- select + f

---
---
---
---
---
---
---
---
---

**This course has been updated and most sections have been archived. 
I'm restarting the entire course from this point forward. Old sections will be available in an archived course somewhere in the future.**

---
---
---
---
---
---
---
---
---

# Section 1: Intro & setup (New UE5 content)
## 1. New Updated UE5 Content
## 2. Welcome To The Course
## 3. Installing Unreal Engine
- https://github.com/EpicGames/UnrealEngine/blob/5.0/Engine/Build/BatchFiles/Linux/README.md
- above instructions worked like a charm for me

```sh
git clone --single-branch --branch 5.0 git@github.com:EpicGames/UnrealEngine.git UnrealEngine-5.0

cd UnrealEngine-5.0
./Setup
./GenerateProjectFiles.sh

make

# If you want to rebuild the editor from scatch, you can use

make UnrealEditor ARGS="-clean" && make UnrealEditor
# this will only rebuild the editor?

# does this also work?
make ARGS="-clean" && make

# run it
cd Engine/Binaries/Linux/
./UnrealEditor


# Or, if you want to start it with a specific project:

cd Engine/Binaries/Linux/
./UnrealEditor "~/Documents/Unreal Projects/MyProject/MyProject.uproject"

```

## 4. Community & Support
## 5. Links & Resources
## 6. Navigating The Viewport
## 7. Moving & Placing Actors
## 8. C++ versus Blueprint

## 9. Helping Us To Help You
(lecture added while following the course so all subsequent numbering is off)

# Section 2: Warehouse Wreckage (New UE5 Content)
https://gitlab.com/GameDevTV/Unreal5CPP/WarehouseWreckage

## 9. Section Intro - Warehouse Wreckage
## 10. Project Setup

- To change the graphical quality:
  - Settings > Engine Scalability Settings


## 11. Blueprint Event Graph
## 12. Physics Simulation

- To enable physics for an object (default = disabled):
  - select object
  - details pane > filters > physics > check "Simulate Physics"

- Gravity (default = enabled)
  - select object
  - details pane > filters > physics > "Enable gravity"

- Mass (default = calculated for us)

## 13. Objects and References

- Open Blueprint for a level:
  - Viewport > Node graph icon dropdown > Open Level Blueprint
  - this opens a new window, we can drag it to our editor window as a new tab

- Select the cube in the Editor
- Blueprint Window > Right click > Create ra eference to cube

## 14. Adding an Impulse
## 15. Blueprint Classes and Instances
## 16. Spawning Actors

- BP: Alt + click REMOVES a connector between nodes

## 17. Data Types
## 18. Pawns and Actor Location
## 19. Control Rotation
## 20. Vector Addition & Multiplication
## 22. Importing Assets

- There's no Epic Game Launcher for Linux
- Lutris works great (https://lutris.net/)

```sh
sudo add-apt-repository ppa:lutris-team/lutris
sudo apt update
sudo apt install lutris
```

- In Lutris, install the "Epic Games Store"
- If error "fsync enabled kernel not found": disable fsync (Epic Games Store advanced settings > disabled fsync)

- Make our project visible for the Epic Games Launcher so we can download assets for it:

```sh
ln -sf \
    ~/online-courses/udemy/unreal-engine-cpp-developer/section-2-warehouse-wreckage/WarehouseWreckage \
    ~/Games/epic-games-store/drive_c/users/ivo/Documents/Unreal\ Projects/
```
- the Epic Games Store will only look in `ivo/Documents/Unreal Projects/` for projects (but this can apparantly be configured, see https://forums.unrealengine.com/t/ue4-launcher-default-save-directory/617/12)
- from the Epic Games Launcher/Store, install the asset "Industry Props Pack 6". This will install our assets under our project under Content

**Another way to import assets**
- Download the zip attached to this lecture
- Unzip, open .uproject file with UE5
- Content Drawer > Content
- we see the IndustryPropsPack6 folder
- right click > Migrate
- check all > ok
- navigate to the project folder we want to put the assets in > Content > Select Folder

- open the showcase level contained within the asset pack: `/Content/IndustryPropsPack6/Maps/Showcase.umap`
- this will take a while, compiling shaders, whatever

- open our initial map again: `/Content/StarterContent/Maps/Minimal_Default.umap`
- to use assets from the asset pack:
  - we can just drag them into our map from `/Content/IndustryPropsPack6/Meshes/`
  

- don't forget to enable physics for our new objects ;)

## 23. Geometry Brushes (BSP)

- create new level:
  - File > New Level > Basic

- delete the floor tile
- Toolbar > Quickly add to the project dropdown > Place Actors Panel
- tab Geometry
- drag Box into the map > rename to Walls
- to scale (don't touch the scale properties if we want our textures to look good later):
  - Brush settings:
    - X = 34*100
    - Y = 27*100
    - Z = 7.6*100
- duplicate the Walls (CTRL+C, CTRL+V), rename to WallsSubtract
- select Walls again to make it slightly bigger (and we can carve out our room with the WallsSubtract brush)
  - X = +200
  - Y = +200
  - Z = +200
- WallSubtract > Brush Settings > Brush Type = Subtractive
- now we have our room!
- save the map: CTRL+S, folder Content, name = Main

- to configure our editor to open this map by default:
  - Settings > Project Settings > Maps & Modes
    - Editor Startup Map = Main
    - Game Default Map = Main

- add a Rack to the map

- to create windows:
  - create new Box Brush
  - Brush Settings > Brush Type = Subtractive
  - rename to Window
  - move it into the wall
  - we have our window!

- to duplicate:
  - CTRL+C, CTRL+V **OR** ALT+drag & release ALT

- select all Windows in the Outline > right click > Move To > New Folder > Windows

## 24. Materials and Lighting

- to copy a Blueprint from another map:
  - open our Minimal_Default map
  - go to Blueprint
  - CTRL+A to select all, CTRL+C
  - go back to our Main map
  - open it's Blueprint
  - delete all default nodes
  - CTRL+V

- simply drag materials to the surface we want to texturize

## 25. Actor Components

- to combine multiple assets into 1 single unit:
  - select the "parent" component (i.e. the rack in our level)
  - find the component you want to add in the Content Drawer (i.e. IndustryPropsPack6/Meshes/SM_Rack02.SM_Rack02)
  - drag it to the StaticMeshComponent of the Rack (under Details)
  - this will create a child

- don't forget to enable physics for the root StaticMeshComponent! (but only on the root StaticMeshComponent, or else we'll get all kinds of weirdness)

## 26. Collision Meshes

- remove complex mesh, add new simple mesh: 
  - open original object from `/Content/IndustryPropsPack6/Meshes/SM_Barrel01.uasset`
  - Set "Lit" to "Player Collision"
  - we can see the original complex mesh now
  - Collision > Remove Collision
  - Collision > Add 10DOP-Z Simplified Collision
  - this should fix any problems when stacking multiple barrels
  - (for me, they still "jump" a little after stacking so not sure what's wrong)

## 27. Variables
## 28. Booleans and Branches
## 29. Functions

- to organize our Blueprint:
  - we can select one or more blocks in a Blueprint, press c to add a comment block
  - a better way is to create functions:
    - select one or more blocks > right click > Collapse to function

## 30. Return Types
## 31. Pure Functions

**What is a side effect?**
- When a function has an observable effect
- E.g.
  - Print String
  - Add Impulse
  - Set Ammo

**What is a pure function?**
- A function with no side effects
- Only return values
- E.g.
  - Get Ammo
  - Get Actor Forward Vector
  - Multiple, minus, greater

## 32. Member Functions
## 33. Loading Levels & Delay Nodes
## 34. Wrap-up and Recap

# Section 3: Obstacle Assault
## 35. Section Intro - Obstacle Assault
## 36. Project Setup

Asset packs:
- https://www.unrealengine.com/marketplace/en-US/product/unreal-learning-kit-games
- https://www.unrealengine.com/marketplace/en-US/product/stylized-male-character-kit-casual

- the Unreal Learning Kit asset allows us to create a project with it instead of importing it
- I "bought" the asset packs above first using a browser (since the Epic Crap Launcher did not open the asset pages)
- open Lutris > Epic Games Store > Library
- Download Unreal Learning Kit: Games
- Create a project with it (creates project in ~/Games/epic-games-store/drive_c/users/ivo/Documents/Unreal\ Projects/)
- Copy the whole project to our repo dir: 
  - `cp -R ObstacleAssault /home/ivo/learning/udemy/unreal-engine-cpp-developer/section-3-obstacle-assault/`
- right click on section-3-obstacle-assault/ObstacleAssault/ObstacleAssault.uproject and change the engine version to 5.0 or whatever
- open the project in the Linux Unreal Editor
- save current level as Main so we don't modify the example level
- set the default level to Main: Settings > Project Settings > Maps & Modes
    - Editor Startup Map = Main
    - Game Default Map = Main

- create symlink from our repo project dir back to 

```sh
ln -sf \
    ~/online-courses/udemy/unreal-engine-cpp-developer/section-3-obstacle-assault/ObstacleAssault \
    ~/Games/epic-games-store/drive_c/users/ivo/Documents/Unreal\ Projects/
```

- close the Lutris Epic Games Launcher first to make sure it reads the refreshed content of `Unreal\ Projects/`
- Open the Epic Games Launcher again via Lutris, should see the Linux project now under "My Projects" with version "Other"
- Click "Add To Project" under the "Stylized Character Kit: Casual 01" asset pack in your vault
- Check "Show all projects"
- Click the "ObstacleAssault" with version "Other" and set the version to 5.0
- "Add to project" will start the download of the asset pack to our symlinked Linux project

- Editor: drag `/Content/SCK_Casual01/Blueprints/ThirdPersonCharacter.uasset` into the map

## 37. Customizing The Character

- we don't want to modify the character in the scene but create a Blueprint instead
- select the char in the scene > Details > Blueprint dropdown > Create Child Blueprint Class
  - name = BP_ThirdPersonCharacter
  - path = /Content

- delete the PlayerStart from the main island in the map (this starts with a different model)
- BP_ThirdPersonCharacter Blueprint:
  - Auto Possess PLayer = Player 0

- We now spawn into our new BP_ThirdPersonCharacter if we play the map

- Some "bugs":
  - the left arm is hanging lower
  - we can't mouse-look left & right

- open the original ThirdPersonCharacter BP from `/Content/SCK_Casual01/Blueprints/ThirdPersonCharacter.uasset`
- we can see some compiler errors:
```
Input Axis Event references unknown Axis 'TurnRate' for  InputAxis TurnRate
Input Axis Event references unknown Axis 'Turn' for  InputAxis Turn
```
- to fix: 
- find node "InputAxis TurnRate", this should also display WARNING to denote the compiler error
- create a new node next to it: "InputAxis TurnRightRate"
- move the connections from the original "InputAxis TurnRate" to "InputAxis TurnRightRate" while holding CTRL
- delete "InputAxis TurnRightRate" and replace it with the new "InputAxis TurnRightRate" node


- find node "InputAxis Turn", this should also display WARNING to denote the compiler error
- create a new node next to it: "InputAxis TurnRight"
- move the connections from the original "InputAxis Turn" to "InputAxis TurnRight" while holding CTRL
- delete "InputAxis Turn" and replace it with the new "InputAxis TurnRight" node

- to change the look of our character:
  - open BP_ThirdPersonCharacter
  - select component Mesh
  - Details > Mesh > Skeletal Mesh

## 38. Compilers and Editors
## 39. PC - Installing Visual Studio
## 40. Mac - Installing XCode
## 41. Installing VSCode

- extensions:
  - C/C++ by Microsoft
  - Unreal Engine 4 Snippets by CAPTINCAPS

## 42. Compiling a C++ Project

- set code editor:
  - Edit > Editor Preferences > General > Source Code
  - Source Code Editor: Visual Studio Code
  - restart if asked

- Editor > Tools > New C++ Class
- select Actor
- Next >
- Name = MovingPlatform
- Create Class

- We have to rebuild the editor (?) after this
- VSCode > Build > Run Build Task > ObstacleAssaultEditor Linux Development Build
- the instructor mentions it would take a long time but for me it took 6.7 seconds. Maybe because I already built the editor from source on Linux?
- relaunch project in UnrealEditor

```sh
cd /media/ivo/ssdata2/UnrealEngine-5.0/Engine/Binaries/Linux
./UnrealEditor ~/online-courses/udemy/unreal-engine-cpp-developer/section-3-obstacle-assault/ObstacleAssault/ObstacleAssault.uproject
```

- UnrealEditor > Content Drawer > C++ Classes (I already had this directory before recompiling the editor)
- our C++ class: `ObstacleAssault/MovingPlatform`
- we can even drag it into the map
- it spawns at 0, 0, 0 though

- git reports over 1100 changes after this lecture: WTH WTH?
- I noticed a new file in the project directory: `.ignore`:
```sh
Content
Saved
Intermediate
.vscode
```
- copied these lines over to my `.gitignore` et voila, down to 41 changes
- disabled `Content` in my `.gitignore` for now since we save our changes to the `Content` directory

## 43. UPROPERTY Variables

Workflow for compilation:
- make change in VSCode, SAVE
- UnrealEditor > LiveCode button (Recompiles game code on the fly)
  - or press CTRL+SHIFT+ALT+P (I added F12 as a shortcut too) 
- Errors appear in OutputLog drawer
  
## 44. C++ Files & BeginPlay
## 45. Using Structs In C++
## 46. Calling Functions in C++
## 47. Tick
## 48. Local Variables
## 49. Pseudo Code
## 50. Function Return Values
## 51. Velocity & DeltaTime
## 52. Scope Resolution Operator
## 53. If Statements
## 54. Using Member Functions
## 55. Blueprint Child Classes
## 56. Forcing Character Collisions
## 57. GameMode

**Game Mode**: An actor that controls the "rules". e.g. Who should spawn where.

- UnrealEditor > Blueprint Toolbar
  - Project settings - GameMode: New ...
    - project-wide settings
  - World override - GameMode: Edit BP_LearningKitGameMode
    - override for this particular level


- UnrealEditor > Blueprint Toolbar > Project settings - GameMode: New ...
  - Create > GameModeBase
  - Path = Content
  - Name = BP_ObstacleAssaultGameMode
  - Save

- This then opens up "BP_ObstacleAssaultGameMode"
- Class Defaults
  - Classes
  - Default Pawn Class = BP_ThirdPersonCharacter
  - Save

- UnrealEditor > Blueprint Toolbar > World override - Select GameModeBase Class = None

- This still does not work when using "Play from here"

- Remove the current character from the level
- Open the "Place Actors Panel" (from the "Quickly add to the project" toolbar button)
- Drag in a "Player Start" actor

- Works and also "Play from here" works

## 58. Writing To The Output Log
- https://docs.unrealengine.com/4.27/en-US/API/Runtime/Core/Logging/ELogVerbosity__Type/
- https://www.cplusplus.com/reference/cstdio/printf/

**NOTE**:
UnrealEditor also configures the UnrealEditor dir in VSCode for a myriad of build tasks.
After moving a project, or changing the UnrealEditor directory, make sure to regenerate the VSCode project files:
- UnrealEditor > Tools > Refresh Visual Studio Code Project

- `` (tilda tilda or ~~) also opens the Log Drawer
- When using the VSCode extension "Unreal Editor Snippets", we can use the shortcut `ulog` for a template UE_LOG string

## 59. FString

- don't forget to prepend the FString variable in UE_LOG with a *

## 60. Member Functions
## 61. Return Statements
## 62. Const Member Functions

- to make a method pure in C++ (i.e. it does not change anything, only return values) we add `const` at the end of the function definition
```cpp
bool AMovingPlatform::ShouldPlatformReturn() const
{
  // ...
}
```
- any method called from a `const` method, must also be marked `const`

## 63. FRotator

To add a new type of MovingPlatform:
- Content drawer > C++ Classes > Moving Platform > right click > Create Blueprint class based on MovingPlatform

## 64. Level Design & Polish

My approach
- What elements can I play with?
  - rotating/moving
  - up/down, left/right
  - pushing/transporting
- Simple linear course
- Get to the end

- Design on paper to limit scope

- Our character cannot get up the stairs because its angle is too high
- we can change the angle our character can move up a slope:
  - open BP_ThirdPersonCharacter
  - under Components, select Character Movement 
  - set Walkable Floor Angle to 60 deg

## 65. Obstacle Assault: Wrap-Up

# Section 4: Crypt Raider
## 66. Section Intro - Crypt Raider
## 67. Project Setup
https://www.unrealengine.com/marketplace/en-US/product/a5b6a73fea5340bda9b8ac33d877c9e2

```sh
cd /mnt/ssdata2/UnrealEngine-release/Engine/Binaries/Linux
./UnrealEditor ~/online-courses/udemy/unreal-engine-cpp-developer/section-4-crypt-raider/CryptRaider/CryptRaider.uproject
```

```sh
ln -sf \
    ~/online-courses/udemy/unreal-engine-cpp-developer/section-4-crypt-raider/CryptRaider \
    ~/Games/epic-games-store/drive_c/users/ivo/Documents/Unreal\ Projects/
```

## 68. Modular Level Design

- turn off ambient lighting: select "unlit" in the perspective view
- 4-pane view: shows perspective, back view wireframe, right view wireframe, top view wireframe

## 69. Modular Level Layout
## 70. Solution: Modular Level Layout
## 71. Light Types

For ambient lighting/sun:
- directional light: shines parallel light like the sun does, location doesn't matter, only rotation matters
- skylight: light comes from all directions
- Content drawer > Settings > Show Engine Assets
- click Engine folder
- search for `BP_Sky_Sphere` and drag it into our level
- Skylight > Details > Sky Light > Recapture scene (to update the light being generated)
- link the directional light to the skylight:
  - Skylight > Details > Directional Light Actor > Directional Light
  - by linking these 2 together we're saying: the sky sphere should match the angle of the directional light
- Skylight > Details > Sky Light > Recapture scene (to update the light being generated)

## 72. Lumen & Light Bleed

- our outside wall has some weird shadowing going on. This is because of some properties of the material not being supported (yet) by Unreal's new lighting system called Lumen
- to fix our outside wall, we need to edit the material:
  - select a wall piece
  - Details > Materials > M_StoneWall_Inst > Browse to 'M_StoneWall_Inst' in Content drawer
  - right click `M_StoneWall_Inst`in the Content Drawer > Find Parent (this is the actual master material)
  - open `M_Tiling_Master`
  - unlink `Pixel Depth Offset` on the right most output box (alt+click)
  - Toolbar > Apply
  - the weird shadow effect has gone away

## 73. Level Lighting

- drag a `BP_Torch` into the level
- to make our own version of this blueprint & change the lighting properties:
  - Outliner > BP_Torch
  - Details > Blueprint button
  - Create Child Blueprint Class
  - Blueprint Name: BP_Torch_Dungeon
  - Path: Content
  - Select

- Outliner > BP_Torch_Dungeon
- (set grid snapping to 10 to place it on the wall properly)
- BP_Torch_Dungeon > Details
  - Torch_PointLight component > Cast Shadows = true
  - SM_Torch component > Dynamic shadow = false
  - (SM_Torch component > Static shadow = false)? No longer visible after the above change
  - Torch_PointLight component > Intensity = 500
  - Torch_PointLight component > Attenuation Radius = 800

- To apply our changes to the blueprint (our prefab in Unity terms):
  - Outliner > BP_Torch_Dungeon > Details > Blueprint button > Apply Instance Changes to Blueprint

- Another type of light: BP_Chandelier
- Outliner > BP_Chandelier > Details > BP_Chandelier (Self) component > Add Point Light = true
- BP_Chandelier (Self) component > Point Light Attenuation Radius = 1000

## 74. Character Blueprint

- we're falling through the floor currently because there are no collisions set up on our floor meshes
- Outliner > SM_Floor (any outside floor panel in the level will do)
- Details > Static Mesh > Browse to 'SM_Floor' in Content Browser
- open it up
- Show button > Simple Collisions
- to add a collision:
  - Collision toolbar button > Add Box Simplified Collision
  - this adds a collision box around our mesh WITH 0 THICKNESS

- to edit the thickness to make sure we don't fall through cracks:
  - Details > Collisions > Primitives > Boxes
  - Z-extent = 10
  - this will put 10 units on TOP of our floor

- we don't want our character to move above the floor:
  - Details > Collisions > Primitives > Boxes
  - Center Z = -10
  - now we have a 10 cm box UNDER the floor
  - Save


- to modify our character:
  - Content Drawer > BP_FirstPersonCharacter > right click > Create Child Blueprint Class
  - name = BP_Player
  - drag to move to Content

- open BP_Player
- select component CharacterMesh1P
- Details > Mesh > Skeletal Mesh > Clear (from the selection dropdown)
- hands should be gone now

- create a new game mode:
  - Toolbar > Blueprint icon > Project settings > GameMode > Create > CryptRaiderGameMode
  - name = BP_CryptRaiderGameMode
  - path = Content
  - Save

## 75. Inheritance vs Composition
## 76. C++ Actor Component

- add "moving" door
- Details > Add > New C++ Component
- Actor Component
- Next >
- Name = Mover
- Create Class

## 77. Pointer Types & GetOwner()
## 78. Deferencing & Arrow (->) Operator

Example code:
```cpp
float MyFloat = 5;
float* YourFloat = &MyFloat;
float FloatValue = *YourFloat;
```


**`*` after type**
```cpp
float* MyFloat
```
which tells us it's a float pointer, a pointer to a float

**`*` before a pointer expression**
```cpp
float FloatValue = *YourFloat;
```
get me the value at this location (aka dereferencing)

## 79. Linkers, Headers and Includes
## 80. FMath::VInterpConstantTo
- https://docs.unrealengine.com/5.0/en-US/API/Runtime/Core/Math/FMath/VInterpConstantTo/

## 81. Scene Components
## 82. Line Tracing & Sweeping
https://docs.unrealengine.com/5.0/en-US/collision-in-unreal-engine/

- Settings > Project settings > Engine > Collision
- Trace channels > New trace channel
- Name = Grabber
- Default Response = Ignore
- Accept


- on an object, we can configure how it should respond to which Trace Channel:
  - Gargoyle > Details > Collision > Collision Preset = Custom
  - Collision Responses > Trace Responses
  - (if we add a new Trace Channel while this object details are open, we have to deselect the object to refresh the Trace Channels list)
  - Grabber: Block

## 83. GetWorld()
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Engine/UWorld/

## 84. DrawDebugLine()
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/DrawDebugLine/

## 85. References vs Pointers

![pointers-vs-references.png](pointers-vs-references.png)

```cpp
float Damage = 0;
float& DamageRef = Damage;
DamageRef = 3;
UE_LOG(LogTemp, Display, TEXT("Damage: %f / DamageRef: %f"), Damage, DamageRef);
// Damage: 3.0000 / DamageRef: 3.0000
```

![ampersand-and-star-symbols-in-context.png](ampersand-and-star-symbols-in-context.png)

## 86. Const References & Out Parameters

```cpp
void UGrabber::DoSomething()
{
	  // ...

    float Damage;
    if (HasDamage(Damage)) 
    {
        PrintDamage(Damage);
    }
}

void UGrabber::PrintDamage(const float& Damage)
{
    UE_LOG(LogTemp, Display, TEXT("Damage: %f"), Damage);
}
    
bool UGrabber::HasDamage(float& OutDamage)
{
    OutDamage = 5;
    return true;
}
```

## 87. Geometry Sweeping
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Engine/UWorld/SweepSingleByChannel/

- to get the Trace Channel name:
  - open `section-4-crypt-raider/CryptRaider/Config/DefaultEngine.ini`
  - search for `Grabber`
  - `+DefaultChannelResponses=(Channel=ECC_GameTraceChannel2,DefaultResponse=ECR_Ignore,bTraceType=True,bStaticObject=False,Name="Grabber")`
  - Trace Channel name = `ECC_GameTraceChannel2`

## 88. Input Action Mappings

- Project settings > Engine > Input

## 89. Blueprint Callable

- open BP_Player
- Grabber component C++ script is added under First Person Camera Component so it moves with our camera
- we can call C++ functions from Blueprint
- after adding the function we want to call from Blueprint, there are some steps involved in getting the C++ function visible in BLueprint:
  - (might not be neccessary for you if the C++ functions show up in Blueprint immediately)
  - LiveCode compile our new code
  - rebuilding from Visual Studio Code: CTRL+SHIFT+B (Build task), select "CryptRaiderEditor Linux Development Build"
  - restarting UnrealEditor
  - **NOT SURE WHICH OF THESE STEPS IS ACTUALLY REQUIRED** (after some reboot and editor restarts it then just worked)

- adding another function to our Grabber did not require another restart of everything, only saved the Blueprint after LiveCode recompile

## 91. FindComponentByClass() & nullptr
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/PhysicsEngine/UPhysicsHandleComponent/

- open BP_Player
- Components > Add > PhysicsHandle


```cpp
    GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
```
- this is a `template` function
- the brackets are used for passing in **compile-time** parameters (just like in C#)

## 92. DrawDebugSphere()
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/DrawDebugSphere/

## 93. Grabbing With Physics Handle
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/PhysicsEngine/UPhysicsHandleComponent/

- make sure to set the Gargoyle mobility:
  - Outliner > Gargoyle > Details > Transform > Mobility = Movable
- and make sure physics are on:
  - Outliner > Gargoyle > Details > Physics > Simulate Physics = true

- the grabbing is a bit wonky at the moment: the statue appears to be glued to the ground but if we play and grab it while it is still moving, we can actually get a hold of the statue and walk around with it

## 94. Waking Physics Objects
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Components/UPrimitiveComponent/

- components that are simulating physics will go to sleep for performance sake when they have been still for a certain amount of time
- we need to "wake" it up ;)

- the jitter-effect on the gargoyle when moving forward with it, is caused by collision with the pawn
- to fix:
  - Outliner > Gargoyle > Details > Collision > Collision Presets > Object Responses
    - set Pawn to Overlap (instead of Block)
  - the dirt mound under the gargoyle has it's own collision body:
    -  Outliner > Gargoyle > Details > SM_Dirt_Mound_A > Collision > Collision Presets > set to No Collision

## 95. Returning Out Parameters
## 96. Overlap Events
https://www.unrealengine.com/en-US/blog/collision-filtering

## 97. Constructors

- the c++ class constructor gets called in the editor, even before play!
- the c++ class constructor even gets called when adding the class to an actor
- **ergo: we need to be mindful of what we put in a constructor**

## 98. TArray
## 99. While & For Loops
## 100. Range Based For Loops
## 101. Actor Tags
## 102. Early Returns


## 104. Dependency Injection
(lecture added while following the course so numbering changed)

## 105. Casting & Actor Attachment
- https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/GameFramework/AActor/AttachToComponent/
- https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Components/UPrimitiveComponent/SetSimulatePhysics/

## 106. Adding and Removing Tags
## 107. Boolean Logical Operators
## 108. Level Polish

- the darkness when not having a light directly in our view is because of the exposure composition method being used
- to change that:
  - Toolbar Quickly add to the project > Volumes > PostProcessVolume
  - right click, Move to > Global folder
  - PostProcessVolume > Details > Exposure
    - check Metering Mode, set to Auto Exposure Basic
  - move the PostProcessVolume to somewhere in the middle of our level (easy to do in 4-pane view)
  - PostProcessVolume > Details > Brush Settings
  -  set x, y, z so the box encompasses our entire level

- add SM_Cell_Side to the mausoleum
- position it in the door on the floor
- take note of the Z position (-450 for me)
- move it up so it no longer blocks the door
- take note of the Z position (0 for me)
- move it back to Z -450 again
- Add component > Mover
- Mover > Move Offset > Z = 450

- add SM_Statue_Stand
- add SM_Statue_Hooded_Metal
- SM_Statue_Hooded_Metal > Details > lock scale, set all to 6
- position statue on stand

- SM_Statue_Hooded_Metal > Details > Collision Presets > Custom ...
  - Grabber = Block
  - Pawn = Ignore
  - check Generate Overlap Events

- SM_Statue_Stand > Details > Add component > Trigger
- reposition trigger so that it encompasses the statue
- SM_Statue_Stand > select Trigger component > Details
  - Trigger Component > Acceptable Actor Tag = Unlock2

- SM_Statue_Hooded_Metal > Details > Actor > Tags
  - add tag = Unlock2

- make sure SM_Statue_Stand is selected in the level
- Toolbar Open Level Blueprint
- right click anywhere in the empty space
- Create a Reference to SM_Statue_Stand
  - drag off right pin > Get Component By Class
    - Component Class = Trigger Component

- back to the level, select cell door (SM_Cell_Side)
- back to Level Blueprint
- right click anywhere in the empty space
- Create a Reference to SM_Cell_Side5
 - drag off right pin > Get Component By Class
    - Component Class = Mover

- pull off of the Return Value pin of Trigger Component block > Set Mover
  - at the first try "Set Mover" did not show up
  - after removing all the added Blueprint blocks, removing and re-adding the Trigger Component to the stand, "Set Mover" showed up
- connect Set Mover block Mover pin to Return Value pin of Mover block
- connect Set Mover block execution pin to Event BegintPlay block execution pin

- back to the level
- SM_Cell_Side5 > Details > General > Mobility > Movable
- SM_Statue_Hooded_Metal > Details > General > Mobility > Movable


```sh
Blueprint Runtime Error: "Accessed None trying to read property Trigger". Node:  Set Mover Graph:  EventGraph Function:  Execute Ubergraph BP Secret Wall Blueprint:  BP_SecretWall
```

```sh
LoginId:59e1db626f444ca38c66512ce573d1cd-000003e8
EpicAccountId:

Caught signal 11 Segmentation fault

libUnrealEditor-CryptRaider.so!UMover::SetShouldMove(bool) [/home/ivo/online-courses/udemy/unreal-engine-cpp-developer/section-4-crypt-raider/CryptRaider/Source/CryptRaider/Mover.cpp:48]
libUnrealEditor-Engine.so!FActorComponentTickFunction::ExecuteTick(float, ELevelTick, ENamedThreads::Type, TRefCountPtr<FGraphEvent> const&) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Engine/Private/Components/ActorComponent.cpp:1070]
libUnrealEditor-Engine.so!FTickFunctionTask::DoTask(ENamedThreads::Type, TRefCountPtr<FGraphEvent> const&) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Engine/Private/TickTaskManager.cpp:284]
libUnrealEditor-Engine.so!TGraphTask<FTickFunctionTask>::ExecuteTask(TArray<FBaseGraphTask*, TSizedDefaultAllocator<32> >&, ENamedThreads::Type, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/Runtime/Core/Public/Async/TaskGraphInterfaces.h:975]
libUnrealEditor-Core.so!FNamedTaskThread::ProcessTasksNamedThread(int, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Core/Private/Async/TaskGraph.cpp:753]
libUnrealEditor-Core.so!FNamedTaskThread::ProcessTasksUntilIdle(int) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Core/Private/Async/TaskGraph.cpp:652]
libUnrealEditor-Core.so!FTaskGraphCompatibilityImplementation::ProcessThreadUntilIdle(ENamedThreads::Type) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Core/Private/Async/TaskGraph.cpp:2105]
libUnrealEditor-Engine.so!FTickTaskSequencer::ReleaseTickGroup(ETickingGroup, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Engine/Private/TickTaskManager.cpp:581]
libUnrealEditor-Engine.so!FTickTaskManager::RunTickGroup(ETickingGroup, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Engine/Private/TickTaskManager.cpp:1591]
libUnrealEditor-Engine.so!UWorld::Tick(ELevelTick, float) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Engine/Private/LevelTick.cpp:1546]
libUnrealEditor-UnrealEd.so!UEditorEngine::Tick(float, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Editor/UnrealEd/Private/EditorEngine.cpp:1775]
libUnrealEditor-UnrealEd.so!UUnrealEdEngine::Tick(float, bool) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Editor/UnrealEd/Private/UnrealEdEngine.cpp:471]
UnrealEditor!FEngineLoop::Tick() [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Launch/Private/LaunchEngineLoop.cpp:5209]
UnrealEditor!GuardedMain(char16_t const*) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/./Runtime/Launch/Private/Launch.cpp:185]
libUnrealEditor-UnixCommonStartup.so!CommonUnixMain(int, char**, int (*)(char16_t const*), void (*)()) [/mnt/ssdata2/UnrealEngine-release/Engine/Source/Runtime/Unix/UnixCommonStartup/Private/UnixCommonStartup.cpp:264]
libc.so.6!__libc_start_main(+0xf2)
UnrealEditor!_start()
```

- eventually I was able to solve above problems by removing and re-adding the BP_SecretWall completely (and fixed it on win since I did not want to wait 5 minutes editor start time on Linux before trying the next possible solution)

## 109. Crypt Raider: Wrap-Up

Extensions to the project:
- add more levels
- add a different sort of puzzle
- trigger that measures weight
- rotating movers
- movers bridges to other zones
- ...


# Section 5: Toon Tanks (v2)

```sh
cd /mnt/ssdata2/UnrealEngine-release/Engine/Binaries
./UnrealEditor ~/online-courses/udemy/unreal-engine-cpp-developer/section-5-toon-tanks-v2/ToonTanks/ToonTanks.uproject
```

## 110. Differences Between UE5 and UE4

- not much difference under the hood
- some UI improvements

## 111. Project Intro
## 112. Pawn Class Creation

![unreal-class-types.png](unreal-class-types.png)

- we'll create a Pawn base class for our tank and our enemy tower: BasePawn

## 113. Creating Components

![section-5-toon-tanks-v2/components.png](section-5-toon-tanks-v2/components.png)

- Content drawer > right click > Blueprint class
  - Actor
  - name = ComponentsActor
- double click ComponentsActor
- Components > Add
  - Capsule Collision
- make our Capsule the new root component by dragging it onto DefaultSceneRoot

- Components > Add
  - Static Mesh
  - name = StaticMeshBase
  - set tank base as mesh: Details > Static Mesh > SM_TankBase

- Components > Add
  - Static Mesh
  - name = StaticMeshTurret
  - set tank turret as mesh: Details > Static Mesh > SM_TankTurret

- make sure StaticMeshTurret is a child of StaticMeshBase (should be by default)

## 114. Forward Declarations & Capsule Component

![section-5-toon-tanks-v2/basepawn-tankbp-towerbp.png](section-5-toon-tanks-v2/basepawn-tankbp-towerbp.png)

**Forward declaration**
- header files will be added to the current file by the preprocesor
- including other header files for the needed functionality in every header file will increase the file size
- instead of including the header file for some functionality we want to use, we can use forward declaration
- this will tell the compiler we will add the definitions later
- we can forward-declare `UCapsuleComponent* CapsuleComp` by prepending it with `class`

```cpp
  class UCapsuleComponent* CapsuleComponent;
```

- including `other header files` in a `header file` will also include these `other header files` AGAIN when we want to use functionality from the `header file`. This quickly bloates up our files and gets messy.
- a better way is to include `other header files` ONLY in the files where we need them. So NOT in the `header file` but in the .cpp
- for this we need Forward Declaration: we need to tell our `header file` we will include the needed `other header files` in our .cpp

**Summary**
- Include only what you use in `.cpp`
- We should include as little as possible in `.h`
  - We DON'T need the header to declare a pointer
  - We DO need the header to construct an object
  - We DO need the header to access members
  - We DO need the headder for inheritance

## 115. Forward Declaration & Constructing The Capsule

- compile in VSCode: CTRL+SHIFT+b > ToonTanksEditor Linux Development Build
- recommended to restart UnrealEditor
- drag BasePawn C++ from Content Drawer into the level
- in Details we can see the `CapsuleComp (Caspule Collider)` automatically added as a child of our BasePawn (through its constructor)

## 116. Static Mesh Components
## 117. Deriving Blueprint Classes

- Content drawer > Content > right click > New Folder
  - name = Blueprints

- Content drawer > Content > Blueprints > right click > New Folder
  - name = Pawns

- Content drawer > C++ Classes > ToonTanks
  - right click BasePawn > Create Blueprint class based on BasePawn
  - name = BP_PawnTank
  - path = Content/Blueprints/Pawns

- Content drawer > C++ Classes > ToonTanks
  - right click BasePawn > Create Blueprint class based on BasePawn
  - name = BP_PawnTurret
  - path = Content/Blueprints/Pawns

## 118. Instance vs Default
## 119. Editing Exposed Variables

![section-5-toon-tanks-v2/uproperty-specifiers.png](section-5-toon-tanks-v2/uproperty-specifiers.png)

## 120. Exposing The Components

- when attempting to make a private variable Blueprint-accessible:
```cpp
private:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    int32 VisibleAnywhereInt = 32;
```

```sh
CompilerResultsLog: Error: /home/ivo/online-courses/udemy/unreal-engine-cpp-developer/section-5-toon-tanks-v2/ToonTanks/Source/ToonTanks/BasePawn.h(35) : Error: BlueprintReadWrite should not be used on private members
```  

- there is a way around this:
```cpp
private:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
    int32 VisibleAnywhereInt = 32;
```

![section-5-toon-tanks-v2/uproperty-specifiers-2.png](section-5-toon-tanks-v2/uproperty-specifiers-2.png)


- open BP_PawnTank
- BaseMesh > Details > Static Mesh > SM_TankBase
- TurretMesh > Details > Static Mesh > SM_TankTurret
- CapsuleComp > Details > Shape > 
  - Capsule Half Height = scale to fit (roughly)
  - Capsule Radius = scale to fit (roughly)
- BaseMesh
  - move it down so the bottom of our tank matches up with our Capsule Collider
  - disable grid snapping for more control
- ProjectileSpawnPoint
  - move it in front of the barrel
  - not too close or else we're going to have collisions with the barrel when spawning projectiles

- same steps as above for BP_PawnTurret
- meshes: SM_TowerBase & SM_TowerTurret


- place a BP_PawnTank into the level
- place 4 BP_PawnTurret into the level

## 121. Creating Child C++ Classes

- Content drawer > C++ Classes > BasePawn > right click > Create C++ class derived from BasePawn
  - name = Tank


- now we have a BasePawn child class called Tank but our BP_PawnTank still inherits from BasePawn
- we could just rebuild our BP_PawnTank and let it inherit from Tank but that's just too much work
- an easier way:
  - open BP_PawnTank
  - Class Settings
  - Details > Class Options > Parent Class
    - set to Tank

- to change our camera view:
  - DON'T change the camera position 
    - not the right way to do it: we want the camera to be attached to the spring arm because the spring arm brings alot of useful features: shrinking in when the camera hits a wall, avoid tank view blockage by other world objects, etc.)
  - instead, change the Spring Arm length and rotation
  - length = 600
  - rotation (using rotation mode (E)) = 50deg downwards

## 122. Possessing The Pawn

- by default, none of our pawns is designated to be possessed by a controller
- if we press play, we are controlling the default pawn (built into the engine)
- to designate a pawn to be possessed:
  - select our BP_PawnTank in the world
  - Details > Auto Possess Player > Player0
- There can be only 1 pawn with Player0!

## 123. Handling Input
## 124. Local Offset
## 125. Movement Speed
## 126. Local Rotation
- https://docs.unrealengine.com/4.27/en-US/BlueprintAPI/Utilities/Transformation/AddActorLocalRotation/


- open BP_PawnTank
- Components > select CapsuleComp
- Details > Collision > Collision Presets = BlockAllDynamic

- my tank wouldn't move anymore after this
- apparantly my CapsuleComp was extending below the wheels of the tank and was colliding with the world
- to fix:
  - open BP_PawnTank
  - select BaseMesh
  - drag it below the bottom border of the CapsuleComp

## 127. Casting
## 128. Using the Mouse Cursor

- open a full screen viewport:
  - Window > Viewports > Viewport 2
  - this window can be maximized if not already
  - pressing ALT+P to play in this window crashed my editor ... :(


**Simple versus Complex collisions**

- each object has 2 kinds of collision boxes:
  - simple: this encompasses the entire object with a minimum of lines (more like a big box)
  - complex: this encompasses every little detail of an object (more like a suit)
- to see this visually:
  - Content drawer > Assets > Meshes
  - open SM_TankBase
  - in the viewport > Show > Simple Collisions
  - in the viewport > Show > Complex Collisions

- if possible use Simple Collisions where possible because it uses a lot less computational power to determine collisions

## 129. Rotating the Turret
## 130. The Tower Class

- after adding the C++ class Tower, we also need to change the parent of our Turret Blueprint! (it's still BasePawn)
- open BP_PawnTurret
- (see top right of the window, it says: "Parent class: Base Pawn")
- Class Settings > Parent Class = Tower

## 131. Fire
## 132. Timers
## 133. The Projectile Class
## 134. Spawning The Projectile
## 135. Projectile Movement Component

- don't forget to also set the BP_Projectile as projectile for the Turret
  - open BP_PawnTurret
  - Class Defaults > Details > Combat > Projectile Class = BP_Projectile 


- if the projectile spawns but then is just stuck in mid-air:
  - move the ProjectileSpawnPoint on the respective Blueprint a little more away from the barrel so it doesn't collide with the Pawn the moment is it spawned (and thus never is allowed to move)

## 136. Hit Events
https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Components/UPrimitiveComponent/OnComponentHit/

![section-5-toon-tanks-v2/hit-events.png](section-5-toon-tanks-v2/hit-events.png)

## 137. Health Component

- Add C++ Class > Actor Component > HealthComponent

## 138. Applying Damage
- https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/Kismet/UGameplayStatics/ApplyDamage/
- https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/GameFramework/UDamageType/

## 139. The Game Mode Class

- we're choosing GameModeBase here as the parent because we don't need the extra functionality from GameMode

- New C++ Class > Game Mode Base > Name = ToonTanksGameMode
- Create new folder 'GameMode' under Content/Blueprints
- in Content/Blueprints/GameMode > right click > Blueprint Class > BP_ToonTanksGameMode
- Project Settings > Maps & Modes > Default Mode > Default GameMode = BP_ToonTanksGameMode
- open BP_ToonTanksGameMode
- Details > Classes > Default Pawn Class = BP_PawnTank
- Compile & save

- once we have our GameMode defined, we can remove our current tank pawn from the game
- as long as we have a Player Start in our level, our BP_PawnTank will be spawned

## 140. Handling Pawn Death
## 141. Custom Player Controller

- open BP_ToonTanksPlayerController
- Class Defaults > Details > Mouse Interface > Default Mouse Cursor = Crosshairs
- (we're not showing the mouse cursor in-game yet)

## 142. Starting The Game
## 143. The Start Game Widget

- first we'll create a new Blueprint implementable event in C++ (see `ToonTanksGameMode.h`) and call it from `AToonTanksGameMode::HandleGameStart()`
- ...
- open BP_ToonTanksPlayerController
- right click > Event Start Game
- to test, we can connect a Print String to it and play the game
- create a UI widget:
  - create folder Content/Blueprints/Widgets
  - right click > User Interface > Blueprint Widget
  - name = WBP_StartGameWidget

- I got a different window here first: "Pick Root Widget for New Widget Blueprint"
- after looking at the details on WBP_StartGameWidget the instructor showed for a split second, I saw it probably had to be "User Widget"
- after opening WBP_StartGameWidget, instead of a canvas box like the instructor, I got a completely blank canvas
- the instructor's view showed it already had a widget called "Canvas Panel"
- I manually added that and then I was back in sync with the instructor

- add widget Text
- Text > Details > Content > Text = Get Ready!
- the text widget is anchored to the top left corner so let's move it to the center:
  - Text widget > Details > Anchors > click dropdown > select button where the widget is both horizontally and vertically in the center
  - if we zoom in we can see that the top left corner of our Text widget is now in the middle (if we set Position X & Position Y to 0)
  - to center the text:
    - Details > Alignment > X = 0.5, Y = 0.5

- if we increase the font size to 42, our text is no longer in the center horizontally
- to fix: set Position X = 0

- if we play now, we don't see the widget because we haven't hooked it up:
- open BP_ToonTanksGameMode
- right click > "Create Widget"
- class = WBP_StartGameWidget
- from this node, drag out and
- "Add to viewport" 
- connect the Return Value pin from the Create Widget node to the Target pin of the Add to Viewport node
- connect the execution pins ("Event Game Start" -> "Create Widget" -> "Add to Viewport")

## 144. Countdown Timer

- open WBP_StartGameWidget
- click Graph (top right) to open a Blueprint-style graph for our widget

- CTRL+w to duplicate the selected node in a Blueprint

## 145. Displaying Countdown Time

- Create Branch shortcut: in Blueprint Graph, hold down `b` > left click

## 146. Winning And Losing
## 147. Game Over HUD
## 148. Hit Particles
## 149. Smoke Trail
## 150. Death Particles
## 151. Sounds
## 152. Camera Shake

- Blueprint CameraShake is no longer available in UE5, choose MatineeCameraShake instead
- UE 4.25 - CameraShake,  UE 4.26+ - UCameraShakeBase
- UE 4.25 - ClientPlayCameraShake, UE 4.26+ - ClientStartCameraShake

## 153. Polish And Wrap-Up

- to build the game: Toolbar > Platforms > ...

# Section 6: Simple Shooter

## 154. Section Intro: Simple Shooter
## 155. Project Setup

https://gitlab.com/GameDevTV/UnrealCourse/SimpleShooter
